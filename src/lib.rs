use curl::easy::{Easy, List};
use serde::{Deserialize, Serialize};
extern crate serde_json;
use std::io::Cursor;
pub use xmlrpc::{
    Error as xmlrpcError, Fault as xmlrpcFault, Request, Transport, Value, Value as xmlrpcValue,
    Value::Array as xmlrpcArray, Value::Bool as xmlrpcBool, Value::Int as xmlrpcInt,
    Value::String as xmlrpcString, Value::Struct as xmlrpcStruct,
};

#[derive(Debug, Serialize, Deserialize)]
pub struct RpcError(String);

impl PartialEq for RpcError {
    fn eq(&self, other: &Self) -> bool {
        self.0 == other.0
    }
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(untagged)]
pub enum Error {
    Xmlrpc(RpcError),
    // Serde(serde_json::Error),
    Io(RpcError),
    FindByFieldError {
        match_field_name: String,
        match_field_value: String,
        cause: String,
    },
    NoContactFoundError {
        match_field_name: String,
        match_field_value: String,
    },
    AchieveGoalError {
        contact_id: String,
        goal: String,
        cause: String,
    },
}

impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self)
    }
}

impl PartialEq for Error {
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (Self::Xmlrpc(l0), Self::Xmlrpc(r0)) => l0 == r0,
            (Self::Io(l0), Self::Io(r0)) => l0 == r0,
            (
                Self::FindByFieldError {
                    match_field_name: l_match_field_name,
                    match_field_value: l_match_field_value,
                    cause: l_cause,
                },
                Self::FindByFieldError {
                    match_field_name: r_match_field_name,
                    match_field_value: r_match_field_value,
                    cause: r_cause,
                },
            ) => {
                l_match_field_name == r_match_field_name
                    && l_match_field_value == r_match_field_value
                    && l_cause == r_cause
            }
            (
                Self::NoContactFoundError {
                    match_field_name: l_match_field_name,
                    match_field_value: l_match_field_value,
                },
                Self::NoContactFoundError {
                    match_field_name: r_match_field_name,
                    match_field_value: r_match_field_value,
                },
            ) => {
                l_match_field_name == r_match_field_name
                    && l_match_field_value == r_match_field_value
            }
            (
                Self::AchieveGoalError {
                    contact_id: l_contact_id,
                    goal: l_goal,
                    cause: l_cause,
                },
                Self::AchieveGoalError {
                    contact_id: r_contact_id,
                    goal: r_goal,
                    cause: r_cause,
                },
            ) => l_contact_id == r_contact_id && l_goal == r_goal && l_cause == r_cause,
            _ => false,
        }
    }
}

impl From<xmlrpc::Error> for Error {
    fn from(e: xmlrpc::Error) -> Self {
        Error::Xmlrpc(RpcError(e.to_string()))
    }
}

impl From<std::io::Error> for Error {
    fn from(e: std::io::Error) -> Self {
        Error::Io(RpcError(e.to_string()))
    }
}

/// Keap Application connection info
#[derive(Debug, Clone)]
pub struct KeapApp {
    app_id: Option<String>,
    app_key: Option<String>,
}

impl Default for KeapApp {
    fn default() -> Self {
        KeapApp::new(&None, &None)
    }
}

impl KeapApp {
    pub fn new(app_id: &Option<String>, app_key: &Option<String>) -> Self {
        KeapApp {
            app_id: app_id.to_owned(),
            app_key: app_key.to_owned(),
        }
    }
}
struct CurlTransport(pub Easy);

impl Transport for CurlTransport {
    type Stream = Cursor<Vec<u8>>;

    fn transmit(
        mut self,
        request: &Request,
    ) -> Result<Self::Stream, Box<dyn std::error::Error + Send + Sync>> {
        let mut body = Vec::new();

        // This unwrap never panics as we are using `Vec<u8>` as a `Write` implementor,
        // and not doing anything else that could return an `Err` in `write_as_xml()`.
        request.write_as_xml(&mut body).unwrap();

        let mut list = List::new();
        list.append("Content-Type: text/xml; charset=utf-8")?;
        list.append(format!("Content-Length: {}", body.len()).as_str())?;
        self.0.http_headers(list)?;

        self.0.post_fields_copy(body.as_slice())?;

        // start optional libcurl settings
        // @todo: make this a config
        self.0.verbose(false)?;
        // end optional libcurl settings

        let mut buf = Vec::new();
        {
            let mut tx = self.0.transfer();
            tx.write_function(|data| {
                buf.extend_from_slice(String::from_utf8_lossy(data).as_bytes());
                Ok(data.len())
            })?;
            tx.perform()?;
        }

        Ok(Cursor::new(buf))
    }
}

fn send(app_id: &str, request: &Request) -> Result<xmlrpc::Value, xmlrpc::Error> {
    let mut easy = Easy::new();
    easy.url(&format!("https://{app_id}.infusionsoft.com/api/xmlrpc"))
        .expect("can't set request URL");
    easy.ssl_verify_peer(true)
        .expect("can't set verify_peer true");
    easy.accept_encoding("UTF-8")
        .expect("can't set transfer encoding UTF-8");

    let tp = CurlTransport(easy);

    match request.call(tp) {
        Ok(result) => Ok(result.to_owned()),
        Err(e) => Err(e),
    }
}

pub fn application_enabled(keap_app: &KeapApp) -> Result<xmlrpc::Value, xmlrpc::Error> {
    let request = xmlrpc::Request::new("DataService.getAppSetting")
        .arg(keap_app.app_key.clone().unwrap())
        .arg(xmlrpcString("Contact".to_string()))
        .arg(xmlrpcString("optiontypes".to_string()));

    send(&keap_app.app_id.clone().unwrap(), &request)
}

pub fn achieve_goal(keap_app: &KeapApp, call_name: &str, contact_id: i32) -> Result<(), Error> {
    let app_id = keap_app.app_id.clone().unwrap_or("".to_string());
    let request = xmlrpc::Request::new("FunnelService.achieveGoal")
        .arg(keap_app.app_key.clone().unwrap())
        .arg(app_id.clone())
        .arg(xmlrpcString(call_name.to_string()))
        .arg(xmlrpcInt(contact_id));

    match send(&app_id, &request) {
        Ok(result) => {
            return match result {
                xmlrpcArray(vec) => {
                    if let Some(map) = vec[0].as_struct() {
                        if map
                            .get("success")
                            .unwrap_or(&xmlrpcBool(false))
                            .as_bool()
                            .unwrap_or(false)
                        {
                            Ok(())
                        } else {
                            if let Some(msg) = map.get("msg") {
                                Err(Error::AchieveGoalError {
                                    contact_id: contact_id.to_string(),
                                    goal: call_name.to_string(),
                                    cause: msg.as_str().unwrap_or("¡quién sabe!").to_string(),
                                })
                            } else {
                                Err(Error::AchieveGoalError {
                                    contact_id: contact_id.to_string(),
                                    goal: call_name.to_string(),
                                    cause: "missing failure message".to_string(),
                                })
                            }
                        }
                    } else {
                        Err(Error::AchieveGoalError {
                            contact_id: contact_id.to_string(),
                            goal: call_name.to_string(),
                            cause: format!("expected xmlrpcStruct: /{:?}/", vec),
                        })
                    }
                }
                _ => Err(Error::AchieveGoalError {
                    contact_id: contact_id.to_string(),
                    goal: call_name.to_string(),
                    cause: format!("expected xmlrpcArray: {result:?}"),
                }),
            };
        }
        Err(e) => Err(Error::Xmlrpc(RpcError(e.to_string()))),
    }
}

pub fn find_by_field<'a>(
    keap_app: &'a KeapApp,
    match_field_name: &'a str,
    match_field_value: &'a str,
    want_field_list: Vec<&'a str>,
    page_size: Option<i32>,
    start_page: Option<i32>,
) -> Result<xmlrpc::Value, Error> {
    let field_list: Vec<xmlrpc::Value> = want_field_list
        .iter()
        .map(|s| xmlrpcString(s.to_string()))
        .collect();

    let request = xmlrpc::Request::new("DataService.findByField")
        .arg(xmlrpcString(
            keap_app.app_key.clone().unwrap_or("".to_string()),
        ))
        .arg(xmlrpcString("Contact".to_string()))
        .arg(xmlrpcInt(page_size.unwrap_or(1000)))
        .arg(xmlrpcInt(start_page.unwrap_or(0)))
        .arg(xmlrpcString(match_field_name.to_string()))
        .arg(xmlrpcString(match_field_value.to_string()))
        .arg(xmlrpcArray(field_list));

    send(&keap_app.app_id.clone().unwrap_or("".to_string()), &request).map_or_else(
        |e| {
            Err(Error::FindByFieldError {
                match_field_name: match_field_name.to_string(),
                match_field_value: match_field_value.to_string(),
                cause: format!("{} {}",e.fault().unwrap().fault_string, e.fault().unwrap().fault_code),
            })
        },
        |v| {
            if 0 == v.as_array().unwrap_or(&[xmlrpcArray(vec![])]).len() {
                Err(Error::NoContactFoundError {
                    match_field_name: match_field_name.to_string(),
                    match_field_value: match_field_value.to_string(),
                })
            } else {
                Ok(v)
            }
        },
    )
}

#[cfg(test)]
mod test_find_by_field {
    use super::*;
    use std::env;

    fn get_keap_app() -> KeapApp {
        include!(".keap_config")
    }

    #[test]
    fn test_find_by_field_10() {
        let result = find_by_field(
            &get_keap_app(),
            "_PhoneNumber",
            "(520) 555-1212",
            vec!["FirstName", "Id"],
            None,
            None,
        );

        match result {
            Ok(fields) => {
                assert_eq!(true, fields.as_array().unwrap().first().is_some());
                println!("{:?}", fields);
            }
            Err(e) => {
                println!("{:?}", e);
                assert!(false);
            }
        }
    }

    #[test]
    fn test_find_by_field_20() {
        let result = find_by_field(
            &get_keap_app(),
            "_PhoneNumber",
            "5205551212",
            vec!["FirstName", "Id"],
            None,
            None,
        );
        match result {
            Ok(r) => {
                println!("{:?}", r);
                assert!(false);
            }
            Err(e) => {
                println!("{:?}", e);
                assert!(true);
            }
        }
    }

    #[test]
    fn test_find_by_field_30() {
        let result = find_by_field(
            &get_keap_app(),
            "_PhoeNumber",
            "5205551212",
            vec!["FirstName", "Id"],
            None,
            None,
        );
        match result {
            Ok(_) => {
                assert!(false);
            }
            Err(e) => {
                println!("1 {}", e);
                assert!(true);
            }
        }
    }

    #[test]
    fn test_find_by_field_40() {
        let result = find_by_field(
            &get_keap_app(),
            "_PhoneNumber",
            "(520) 555-1212",
            vec!["_StopRequested"],
            None,
            None,
        );

        match result {
            Ok(fields) => {
                assert_eq!(true, fields.as_array().unwrap().first().is_some());
                println!("{:?}", fields);
            }
            Err(e) => {
                println!("{:?}", e);
                assert!(false);
            }
        }
    }
}

#[cfg(test)]
mod test_application_enabled {

    use super::*;
    use std::env;

    fn get_keap_app() -> KeapApp {
        include!(".keap_config")
    }

    #[test]
    fn test_application_enabled_10() {
        assert_eq!(true, application_enabled(&get_keap_app()).is_ok());
    }
}

#[cfg(test)]
mod test_keap_xmlrpc {
    use super::*;
    use std::env;

    fn get_keap_app() -> KeapApp {
        include!(".keap_config")
    }

    #[test]
    fn test_keap_xmlrpc_00() {
        // static CACERTFILE: &str = "/var/www/ptisms/private/infusionsoft.pem";
        let request = xmlrpc::Request::new("DataService.echo")
            .arg(xmlrpc::Value::String("hello world".to_string()));
        let mut easy = Easy::new();
        easy.url(&format!(
            "https://{}.infusionsoft.com/api/xmlrpc",
            get_keap_app().app_id.unwrap()
        ))
        .expect("can't set request URL");
        easy.ssl_verify_peer(true)
            .expect("can't set verify_peer true");
        // easy.capath(CACERTFILE).expect("can't find the cert file infusionsoft.pem");
        easy.accept_encoding("UTF-8")
            .expect("can't set transfer encoding UTF-8");

        let tp = CurlTransport(easy);
        let result = request.call(tp);

        assert_eq!(true, result.is_ok());
    }

    #[test]
    fn test_keap_xmlrpc_10() {
        let request = xmlrpc::Request::new("DataService.findByField")
            .arg(get_keap_app().app_key)
            .arg(xmlrpcString("Contact".to_string()))
            .arg(xmlrpcInt(1000))
            .arg(xmlrpcInt(0))
            .arg(xmlrpcString("_PhoneNumber".to_string()))
            .arg(xmlrpcString("5205551212".to_string()))
            .arg(xmlrpcArray(vec![
                xmlrpcString("FirstName".to_string()),
                xmlrpcString("Id".to_string()),
            ]));
        let mut easy = Easy::new();
        easy.url(&format!(
            "https://{}.infusionsoft.com/api/xmlrpc",
            get_keap_app().app_id.unwrap()
        ))
        .expect("can't set request URL");
        easy.ssl_verify_peer(true)
            .expect("can't set verify_peer true");
        easy.accept_encoding("UTF-8")
            .expect("can't set transfer encoding UTF-8");

        let tp = CurlTransport(easy);
        let result = request.call(tp);

        assert_eq!(true, result.is_ok());
    }

    #[test]
    fn test_keap_xmlrpc_20() {
        let request = xmlrpc::Request::new("DataService.findByField")
            .arg(get_keap_app().app_key)
            .arg(xmlrpcString("Contact".to_string()))
            .arg(xmlrpcInt(1000))
            .arg(xmlrpcInt(0))
            .arg(xmlrpcString("_PhoneNumber".to_string()))
            .arg(xmlrpcString("5205551212".to_string()))
            .arg(xmlrpcArray(vec![
                xmlrpcString("FirstName".to_string()),
                xmlrpcString("XYZZY".to_string()),
            ]));
        let mut easy = Easy::new();
        easy.url(&format!(
            "https://{}.infusionsoft.com/api/xmlrpc",
            get_keap_app().app_id.unwrap()
        ))
        .expect("can't set request URL");
        easy.ssl_verify_peer(true)
            .expect("can't set verify_peer true");
        easy.accept_encoding("UTF-8")
            .expect("can't set transfer encoding UTF-8");

        let tp = CurlTransport(easy);
        let result = request.call(tp);

        assert_eq!(true, result.is_err());
    }
}

#[cfg(test)]
mod test_achieve_goal {
    use super::*;
    use std::env;

    fn get_keap_app() -> KeapApp {
        include!(".keap_config")
    }

    #[test]
    // "DatabaseObjectNotFoundException: Couldn't find the record with primary key 12 in Contact"
    fn test_achieve_goal_10() {
        let result = achieve_goal(&get_keap_app(), "FRED", 12);

        match result {
            Ok(_) => {
                assert!(false);
            }
            Err(e) => {
                assert_eq!(
                    Error::AchieveGoalError {
                        contact_id: "12".to_string(),
                        goal: "FRED".to_string(),
                        cause:  "DatabaseObjectNotFoundException: Couldn't find the record with primary key 12 in Contact".to_string()
                    },
                    e
                );
            }
        }
    }

    #[test]
    // "No Goals were configured to be achieved by this event."
    fn test_achieve_goal_20() {
        let result = achieve_goal(&get_keap_app(), "FRED", 42727);

        match result {
            Ok(_) => {
                assert!(false);
            }
            Err(e) => {
                assert_eq!(
                    Error::AchieveGoalError {
                        contact_id: "42727".to_string(),
                        goal: "FRED".to_string(),
                        cause: "No Goals were configured to be achieved by this event.".to_string()
                    },
                    e
                )
            }
        }
    }

    #[test]
    // Achieve goal XMLRPC
    fn test_achieve_goal_30() {
        assert_eq!(true, achieve_goal(&get_keap_app(), "XMLRPC", 42727).is_ok());
    }

    #[test]
    // Serialize
    fn test_achieve_goal_40() {
        let result = achieve_goal(&get_keap_app(), "FRED", 42727);

        match result {
            Ok(_) => {
                assert!(false);
            }
            Err(e) => {
                println!("{:?}", &serde_json::to_string(&e));
            }
        }
    }
}
